import pickle
import numpy
from pymprog import *
import time
import copy
import math

# Recommend L movies with the highest predicted rating to each user
def recommendMovie(ratings_matrix, predicted_ratings_matrix, L):
    
    cov = {}
    for movie_id in range(151):
        cov[movie_id] = 0
    rec_movies = []
    
    L_best_movies_rates = {}
    L_best_movies_ids = {}
    for user_id in range(635):
        L_best_movies_rates[user_id] = []
        L_best_movies_ids[user_id] = []
        
    for user_id, user_rate in enumerate(ratings_matrix):
        iterations = 1
        max_id_rates = []
        max_rate = [0.0] * L
        
        while iterations <= L:
            for movie_id, rate in enumerate(user_rate):
                if rate == 0.0:
                    if predicted_ratings_matrix[user_id][movie_id] > max_rate[iterations-1] and movie_id not in max_id_rates:
                        max_rate[iterations-1] = predicted_ratings_matrix[user_id][movie_id]
                        pos = movie_id
            L_best_movies_ids[user_id].append(pos)
            L_best_movies_rates[user_id].append(max_rate[iterations-1])
            max_id_rates.append(pos) # keep movie id with highest rate (for printing)
            rec_movies.append(pos) # list for keeping recommended movies
            iterations += 1
        
#    print L_best_movies_ids
#    print L_best_movies_rates
    
#    pickle.dump(L_best_movies_ids, open("150_best_movies_ids.p", "wb"))
#    pickle.dump(L_best_movies_rates, open("150_best_movies_rates.p", "wb"))
    for movie_id in rec_movies:
        cov[movie_id] += 1
    return cov

def heuristic1(d, cov, L150_best_movies_ids, L150_best_movies_rates, L):
    I_1 = [] 
    I_2 = [] 
    a = 4.1
    # Fill sets I_1, I_2
    for movie_id in cov:
        if cov[movie_id] > d:
            I_1.append(movie_id)
        elif cov[movie_id] < d:
            I_2.append(movie_id)
    
    
    # Calculate cost
    l = 0
    cr = []    
    while l <= L-1:
        cost = {}
        for user_id in range(635):
            cost[user_id] = [100]*151
        for user_id in L150_best_movies_ids:
            CF_rate = L150_best_movies_rates[user_id][l]
            L_best_movies_ids = L150_best_movies_ids[user_id][:L]
            for i, pred_rate in enumerate(L150_best_movies_ids[user_id]):
                movie_id = L150_best_movies_ids[user_id][i]
                if movie_id in L_best_movies_ids:
                    cost_value = 100
                    cost[user_id][movie_id] = cost_value
                else:
                    cost_value = CF_rate - L150_best_movies_rates[user_id][i]
                    cost[user_id][movie_id] = cost_value
        cr.append(cost)
        l += 1

    # Movies Allocations from CF
    allocations = {}
    for user_id in L150_best_movies_ids:
        allocations[user_id] = [L150_best_movies_ids[user_id][i] for i in range(L)]
    
    # Individual cost for each user
    individual_cost = {}
    for user in range(635):
        individual_cost[user] = 0
    
    # Find substitute item-movie with minimum cost 
    l = L-1
    cc = 1
    cost_of_l = cr[l]
    while len(I_2) > 0:
        if cc > 0:
            cc = 0
            min_cost = 100
            for user_id in range(len(L150_best_movies_ids)-1):
                if L150_best_movies_ids[user_id][l] in I_1: 
                    cc += 1
                    for movie_id in I_2:
                        if movie_id not in L150_best_movies_ids[user_id][:L]:
                            if cost_of_l[user_id][movie_id] < min_cost:
                                min_cost = cost_of_l[user_id][movie_id]
                                pos_user = user_id
                                pos_movie = movie_id
            
            
            sub_movie = allocations[pos_user][l]
            cov[sub_movie] -= 1
            if cov[sub_movie] == d:
                I_1.remove(sub_movie)
            cov[pos_movie] += 1
            if cov[pos_movie] == d:
                I_2.remove(pos_movie)
            individual_cost[pos_user] += min_cost
        else:
            if l > 0:
                l -= 1
                cost_of_l = cr[l]
            else:
                print "NO FEASIBLE SOLUTION"
                break
    tot = 0
    for i, user in enumerate(individual_cost):
        tot += a*individual_cost[user]
    print tot, cov
    
def linearProgramCost(d, L150_best_movies_ids, L150_best_movies_rates, L):
    #cost function
    l = 0
    cr = []    
    while l <= L-1:
        cost = {}
        for user_id in range(635):
            cost[user_id] = [100]*151
        for user_id in L150_best_movies_ids:
            CF_rate = L150_best_movies_rates[user_id][l]
            L_best_movies_ids = L150_best_movies_ids[user_id][:L]
            for i, pred_rate in enumerate(L150_best_movies_ids[user_id]):
                movie_id = L150_best_movies_ids[user_id][i]
                if movie_id in L_best_movies_ids:
                    cost_value = 100
                    cost[user_id][movie_id] = cost_value
                else:
                    cost_value = CF_rate - L150_best_movies_rates[user_id][i]
                    cost[user_id][movie_id] = cost_value
        cr.append(cost)
        l += 1
    
    # LINEAR PROGRAMM
    N = len(cost[0])
    K = len(cost)
    p = model("movies_allocation")
    NKL = iprod(range(N), range(K), range(L))
    
    #variables
    #x = p.var('x_iur', NKL, bool)
    x = p.var('x_iur', NKL, bounds=(0,1))
   
    # constraints
    for u in range(K):
        for r in range(L):
            sum(x[i,u,r] for i in range(N)) <= 1

    for i in range(N):
        sum(sum(x[i,u,r] for r in range(L)) for u in range(K)) >= d
    
    # objective function
    p.minimize(sum(sum(sum(cr[r][u][i]*x[i,u,r] for r in range(L)) for u in range(K)) for i in range(N))) 
                
    #p.solver(int, msg_lev=glpk.GLP_MSG_OFF)
    p.solver(float)
    p.solve()
    ip_value = p.vobj()
    print ip_value
    p.save(sol='_save.sol')
    p.end()


def heuristic_fairness(d, cov, L150_best_movies_ids, L150_best_movies_rates, L):
    I_1 = [] 
    I_2 = [] 
    a = 4.1 
    # Fill sets I_1, I_2
    for movie_id in cov:
        if cov[movie_id] > d:
            I_1.append(movie_id)
        elif cov[movie_id] < d:
            I_2.append(movie_id)
    
    # Calculate cost
    l = 0
    cr = []    
    while l <= L-1:
        cost = {}
        for user_id in range(635):
            cost[user_id] = [100]*151
        for user_id in L150_best_movies_ids:
            CF_rate = L150_best_movies_rates[user_id][l]
            L_best_movies_ids = L150_best_movies_ids[user_id][:L]
            for i, pred_rate in enumerate(L150_best_movies_ids[user_id]):
                movie_id = L150_best_movies_ids[user_id][i]
                if movie_id in L_best_movies_ids:
                    cost_value = 100
                    cost[user_id][movie_id] = cost_value
                else:
                    cost_value = CF_rate - L150_best_movies_rates[user_id][i]
                    cost[user_id][movie_id] = cost_value
        cr.append(cost)
        l += 1

    # Movies Allocations from CF
    allocations = {}
    for user_id in L150_best_movies_ids:
        allocations[user_id] = [L150_best_movies_ids[user_id][i] for i in range(L)]
    
    # Individual cost for each user
    individual_cost = {}
    for user in range(635):
        individual_cost[user] = 0
    
        # Find substitute item-movie with minimum cost 
    l = L-1
    cc = 1
    cc2 = 1
    cost_of_l = cr[l]
    users_fairness = []
    while len(I_2) > 0:
        min_cost = 100
        for user_id in range(len(L150_best_movies_ids)-1):
            if L150_best_movies_ids[user_id][l] in I_1: 
                cc += 1
                if user_id not in users_fairness:
                    cc2 += 1
                    for movie_id in I_2:
                        if movie_id not in L150_best_movies_ids[user_id][:L]:
                            if cost_of_l[user_id][movie_id] < min_cost:
                                min_cost = cost_of_l[user_id][movie_id]
                                pos_user = user_id
                                pos_movie = movie_id
        if cc > 0:
            cc = 0
            if cc2 > 0:
                cc2 = 0                        
                sub_movie = allocations[pos_user][l]
                cov[sub_movie] -= 1
                if cov[sub_movie] == d:
                    I_1.remove(sub_movie)
                cov[pos_movie] += 1
                if cov[pos_movie] == d:
                    I_2.remove(pos_movie)
                individual_cost[pos_user] += a*min_cost
                users_fairness.append(pos_user)
            else:
                users_fairness = []
        else:
            if l > 0:
                l -= 1
                cost_of_l = cr[l]
                users_fairness = []
            else:
                print "NO FEASIBLE SOLUTION"
                break
    sum1 = 0 
    sum2 = 0
    for i, user in enumerate(individual_cost):
        sum1 += individual_cost[user] ** 2
        sum2 += individual_cost[user]
    sum1 = sum1/float(635)
    sum2 = sum2/float(635)
    var = sum1 - sum2**2
    print "standard deviation: ", math.sqrt(var)

def linearProgramFairness(d, L150_best_movies_ids, L150_best_movies_rates, L):
    # cost function
    l = 0
    cr = []    
    while l <= L-1:
        cost = {}
        for user_id in range(635):
            cost[user_id] = [100]*151
        for user_id in L150_best_movies_ids:
            CF_rate = L150_best_movies_rates[user_id][l]
            L_best_movies_ids = L150_best_movies_ids[user_id][:L]
            for i, pred_rate in enumerate(L150_best_movies_ids[user_id]):
                movie_id = L150_best_movies_ids[user_id][i]
                if movie_id in L_best_movies_ids:
                    cost_value = 100
                    cost[user_id][movie_id] = cost_value
                else:
                    cost_value = CF_rate - L150_best_movies_rates[user_id][i]
                    cost[user_id][movie_id] = cost_value
        cr.append(cost)
        l += 1
    
    # LINEAR PROGRAMM
    N = len(cost[0])
    K = len(cost)
    p = model("movies_allocation")
    NKL = iprod(range(N), range(K), range(L))
    
    #variables
    #x = p.var('x_iur', NKL, bool)
    z = p.var('z')
    x = p.var('x_iur', NKL, bounds=(0,1))
    
    # constraints
    for r in range(L):
        for u in range(K):
            sum(x[i,u,r] for i in range(N)) <= 1

    for i in range(N):
        sum(sum(x[i,u,r] for r in range(L)) for u in range(K)) >= d
    
    for u in range(K):
        z >= sum(sum(cr[r][u][i]*x[i,u,r] for r in range(L)) for i in range(N)) 

    # objective function
    p.minimize(z)
    
    #p.solver(int)
    p.solver(float)
    p.solve()
    ip_value = p.vobj()
    print ip_value
    p.save(sol='_saveFairness.sol')
    p.end()
    
if __name__ == "__main__":
    
    print "Importing ratings matrix, predicted ratings matrix"
    ratings_matrix = pickle.load(open("ratings_matrix.p", "rb"))
    ratings_matrix = ratings_matrix.transpose()
    
    predicted_ratings_matrix = pickle.load(open("predicted_ratings_matrix.p", "rb"))
    predicted_ratings_matrix = numpy.array(predicted_ratings_matrix, dtype = float)
    
    print "Loading best movies ids for each user"
    L150_best_movies_ids = pickle.load(open("150_best_movies_ids.p", "rb"))
    L150_best_movies_rates = pickle.load(open("150_best_movies_rates.p", "rb"))
    print "Calculating coverage for each movie, L recommended movies"
#    print L150_best_movies_ids
#    print L150_best_movies_rates
    L = 1
    cov = recommendMovie(ratings_matrix, predicted_ratings_matrix, L)
    
#    print "Heuristic for cost"
#    heuristic1(2, cov, L150_best_movies_ids, L150_best_movies_rates, L)

#    print "Heuristic for fairness"
#    heuristic_fairness(20, cov, L150_best_movies_ids, L150_best_movies_rates, L)

    print "Linear solution for total cost"
    linearProgramCost(2, L150_best_movies_ids, L150_best_movies_rates, L)

#    print "Linear solution for fairness cost"
#    linearProgramFairness(1, L150_best_movies_ids, L150_best_movies_rates, L)

import csv
import pickle
import numpy
import math
from scipy import spatial

# parseRatingsData function is used for moviesID_map generation 
def parseRatingsData():
    movies = []
    moviesID_map = {}
    for i in range(9066):
        moviesID_map[i] = "NaN"
        
    f = open("ml-latest-small/ratings.csv", 'rt')
    for i,row in enumerate(csv.reader(f)):
        if i >= 1:
            user = int(row[0])
            movie = int(row[1])
            rating = float(row[2])
            if movie not in movies:
                movies.append(movie)
    cc = 0
    f = open("ml-latest-small/ratings.csv", 'rt')
    for i,row in enumerate(csv.reader(f)):
        if i >= 1:
            movie = int(row[1]) 
            if movie in movies:
                moviesID_map[movie] = cc
                movies.remove(movie)
                cc += 1
    pickle.dump(moviesID_map, open("moviesID_map.p", "wb"))

# matrixInit function creates ratings_matrix format (ratings_matrix: row --> users, collumn --> movies)
def matrixInit():
    ratings_matrix = []
    for i in range(671):
        ratings_matrix.append([])
        for j in range(9066):
            ratings_matrix[i].append(0)
    return ratings_matrix

# fillDataRatings function fills ratings_matrix with ratings, which coming from the dataset
def fillDataRatings(moviesID_map, ratings_matrix):

    f = open("ml-latest-small/ratings.csv", 'rt')
    for i,row in enumerate(csv.reader(f)):
        if i >= 1:
            user = int(row[0])
            movie = int(row[1])
            rating = float(row[2])
            movieId = moviesID_map[movie]
            ratings_matrix[user-1][movieId] = rating
    
    return ratings_matrix

# Calculate mean for each movie row vector
# Need to discuss ...
def meanRatingsMatrix_callculation(ratings_matrix):
    ratings_matrix = numpy.array(ratings_matrix, dtype = float)
    #ratings_matrix = ratings_matrix.transpose() # user collumns, movies rows
    movies_mean = []
    for movie_rating in ratings_matrix:
        non_zero_movie_rates = []
        for rate in movie_rating:
            if rate > 0:
                non_zero_movie_rates.append(rate)
        mean = sum(non_zero_movie_rates)/float(len(non_zero_movie_rates))
        movies_mean.append(mean)

    for i,movie_rating in enumerate(ratings_matrix):
        for j,rate in enumerate(movie_rating):
            if rate > 0:
                ratings_matrix[i][j] = rate - float(movies_mean[i])
    return ratings_matrix

# ItemToItemSimilarity takes ratings_matrix and returns the similiraty of each movie with the others
# We use cosine similarity
def ItemToItemSimilarity(ratings_matrix):
    movies_similarity = []
    for i, movie_A in enumerate(ratings_matrix):
        movie_A_to_others_sim = []
        for j, movie_B in enumerate(ratings_matrix):
            if i == j:
                cos = 1
            else:
                cos = 1 - spatial.distance.cosine(movie_A, movie_B)
            movie_A_to_others_sim.append(cos)
        movies_similarity.append(movie_A_to_others_sim)                
    pickle.dump(movies_similarity, open("movies_similarity.p", "wb"))
    return movies_similarity    

def predictRatingForUser(temp_ratings_matrix, userID, movies_similarity, user_rating):
    for movieId, movie_rate in enumerate(user_rating):
        if movie_rate == 0.0:
            Sum = 0
            Sij = []
            for i, similarity in enumerate(movies_similarity[movieId]):
                if user_rating[i] > 0.0:
                        Sum += similarity * user_rating[i]
                        Sij.append(similarity)
            if (sum(Sij) == 0):
                print(movieId)
            temp_ratings_matrix[movieId][userID] = round(Sum/sum(Sij), 6)
    return temp_ratings_matrix[:,userID]
     
if __name__ == "__main__":

#    print ("Creating ratings matrix format ...")
#    ratings_matrix = matrixInit()
#    #parseRatingsData()
#    moviesID_map = pickle.load(open("moviesID_map.p", "rb"))
#    
#    print ("Filling ratings matrix with ratings coming from dataset ...") 
#    ratings_matrix = fillDataRatings(moviesID_map, ratings_matrix)
#    ratings_matrix = numpy.array(ratings_matrix, dtype = float)
#    ratings_matrix = numpy.delete(ratings_matrix, (44, 75, 203, 221, 226, 268, 288, 295, 300, 321, 330, 365, 376, 396, 443, 503, 525, 539, 580, 637, 641, 644), axis = 0)
#    ratings_matrix = numpy.delete(ratings_matrix, (49, 155, 167, 430, 469, 547, 552, 611), axis = 0)
#    ratings_matrix = numpy.delete(ratings_matrix, (44, 96, 321, 519, 613, 625), axis = 0)
#    ratings_matrix = ratings_matrix.transpose()
#    ratings_matrix = ratings_matrix[:155,:]
#    ratings_matrix = numpy.delete(ratings_matrix, (133, 136, 135, 138, 217), axis = 0)

#    print "Calculating mean for each movie row vector ..."
#    ratings_matrix = meanRatingsMatrix_callculation(ratings_matrix)
#    ratings_matrix = [[1,0,3,0,0,5,0,0,5,0,4,0],
#                      [0,0,5,4,0,0,4,0,0,2,1,3],
#                      [2,4,0,1,2,0,3,0,4,3,5,0],
#                      [0,2,4,0,5,0,0,4,0,0,2,0],
#                      [0,0,4,3,4,2,0,0,0,0,2,5],
#                      [1,0,3,0,3,0,0,2,0,0,4,0]]    
#    mean_ratings_matrix = meanRatingsMatrix_callculation(ratings_matrix)

    print ("Calculating movies similiraty ...")
#    movies_similarity = ItemToItemSimilarity(ratings_matrix)
    movies_similarity = pickle.load(open("movies_similarity.p", "rb"))
    
    print ("Predict ratings for unrated movies")
#    predicted_ratings_matrix = []
#    for userID in range(635):
#        temp_ratings_matrix = numpy.copy(ratings_matrix)
#        user_rating = ratings_matrix[:,userID]
#        predicted_ratings_matrix.append(predictRatingForUser(temp_ratings_matrix, userID, movies_similarity, user_rating))
    
#    pickle.dump(ratings_matrix, open("ratings_matrix.p", "wb"))
    ratings_matrix = pickle.load(open("ratings_matrix.p", "rb"))
#    print ratings_matrix
#    pickle.dump(predicted_ratings_matrix, open("predicted_ratings_matrix.p", "wb"))
    predicted_ratings_matrix = pickle.load(open("predicted_ratings_matrix.p", "rb"))
    predicted_ratings_matrix = numpy.array(predicted_ratings_matrix, dtype = float)
    for user_id, user_rates in enumerate(predicted_ratings_matrix):
        print "Ratings matrix for user (including predicted ratings):", user_id, "are --> \n", user_rates
        print "++++++++++++++++++++++++++++++++++++++++++++++++"

import os
import time
import gzip
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
import matplotlib.pyplot as plt
import scikitplot as skplt
import cPickle as pickle
from mnist import MNIST
from PIL import Image
import math
# from resizeimage import resizeimage 

def load_mnist_train(path, kind='train'):

    """Load MNIST data from `path`"""
    labels_path = os.path.join(path,
                               '%s-labels-idx1-ubyte.gz'
                               % kind)
    images_path = os.path.join(path,
                               '%s-images-idx3-ubyte.gz'
                               % kind)

    with gzip.open(labels_path, 'rb') as lbpath:
        labels = np.frombuffer(lbpath.read(), dtype=np.uint8,
                               offset=8)

    with gzip.open(images_path, 'rb') as imgpath:
        images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                               offset=16).reshape(len(labels), 784)
    return images, labels

def load_mnist_test(path, kind='t10k'):

    """Load MNIST data from `path`"""
    labels_path = os.path.join(path,
                               '%s-labels-idx1-ubyte.gz'
                               % kind)
    images_path = os.path.join(path,
                               '%s-images-idx3-ubyte.gz'
                               % kind)

    with gzip.open(labels_path, 'rb') as lbpath:
        labels = np.frombuffer(lbpath.read(), dtype=np.uint8,
                               offset=8)

    with gzip.open(images_path, 'rb') as imgpath:
        images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                               offset=16).reshape(len(labels), 784)
    # [:1000]
    return images, labels

def load_photo():
    # Load User Photo
    img = Image.open("UserPhoto/2.jpg")
    
    # Reshape photo to 28*28 pixel and greyscale
    img = Image.open("UserPhoto/2.jpg").convert('L')
    img = img.resize((28, 28))
    data = list(img.getdata()) # convert image data to a list of integers
    for i,pix in enumerate(data):
        if pix > 255:
            data[i] = 0
    x = np.array(data)
    x = x.reshape(1, -1)

    WIDTH, HEIGHT = img.size
    # convert that to 2D list (list of lists of integers)
    data = [data[offset:offset+WIDTH] for offset in range(0, WIDTH*HEIGHT, WIDTH)]
    # Here's another more compact representation.
    chars = '@%#*+=-:. '  # Change as desired.
    scale = (len(chars)-1)/255.

    for row in data:
        print(' '.join(chars[int(value*scale)] for value in row))
    print(img.size)
    return x


# Classification Uncertainty
def class_uncertainty(clf, output):
    output = output.reshape(1, -1)
    S = 0
    output_layer_Pr = clf.predict_proba(output)
    output_layer_Pr = list(output_layer_Pr[0])
    for i in range(len(output_layer_Pr)):
        S += output_layer_Pr[i] * math.log(output_layer_Pr[i],2)
    Cl_UN = (-1/float(math.log(10,2)))*S
    return Cl_UN

# Probability mash function of NN Run Time 
def probability_mass_f(clf, x_test):
    NN_time_stats = []
    for image in x_test:
        image = image.reshape(1, -1)
        predict_time_st = time.time()
        clf.predict(image)
        predict_time_end = time.time()
        image_prediction_time = math.ceil((predict_time_end - predict_time_st)*100000)
        NN_time_stats.append(image_prediction_time)
    
    temp = set(NN_time_stats)
    cc = 0
    Pr_mass = {}
    for sec in temp:
         Pr_mass[sec] = NN_time_stats.count(sec)/float(len(x_test))
    return Pr_mass
    
# Building neural network
def MLP(x_train, y_train, x_test, y_test):
    clf = MLPClassifier(solver='adam',
                        activation='relu',
                        batch_size=129,
                        max_iter=100,
                        alpha=1e-6,
                        warm_start=True,
                        hidden_layer_sizes=(100, 100, 100, 100, 100),
                        random_state=1,
                        verbose=True,
                        tol=1e-7,
                        learning_rate_init=0.0005)
                        
    # Train phase
    train_time_st = time.time()
    clf.fit(x_train, y_train)
    train_time_end = time.time()
    print ("==================================")
    print ("Training time:", train_time_end - train_time_st, "sec")
    print ("==================================")
    print ("Classification Uncertainty:", class_uncertainty(clf, x_test[0]))
    # pickle.dump(clf, open('MLP_2classes.p', 'wb')) # store MLP parameters to hard disk
    
    # Prediction for each image. Probability mass function exploitation.
    NN_time_stats = probability_mass_f(clf, x_test)
    print ("==================================")
    print ("Probability mass function:", NN_time_stats)

    # Prediction phase
    predict_time_st = time.time()
    y_pred = clf.predict(x_test)
    predict_time_end = time.time()
    print ("==================================")
    print("Time for classification results:", predict_time_end - predict_time_st, "sec")
    print("Average time for single image classification:", (predict_time_end - predict_time_st)/float(len(x_test))) 
    print ("==================================")

    print ("Precision , Recall, F1 score Metrics")
    print (metrics.classification_report(y_test, y_pred))
    
    print ("==================================")
    print("Classification Accuracy:", clf.score(X_test, Y_test, sample_weight=None))
    print ("==================================")
    print ("")

    # Learning curves
    y_probas = clf.predict_proba(x_test)
    skplt.metrics.plot_precision_recall_curve(y_test, y_probas, curves=['each_class'])
    plt.show()
    skplt.metrics.plot_precision_recall_curve(y_test, y_probas, curves=['micro'])
    plt.show()
    skplt.estimators.plot_learning_curve(clf, x_train, y_train)
    plt.show()
    metrics.classification_report(y_test, y_pred)

# Prediction(T-shirt or trouser) for random photo
def predict(mlp, x_test):
    clothes_id = {0: 'T-shirt/top',
                  1: 'Trouser'}
                  
    y_pred = mlp.predict(x_test)
    all_probs = mlp.predict_proba(x_test)
    print("The category is: ", clothes_id[y_pred[0]])
    print("")
    print("Categories Probabilities:")
    print ('Pr(' +clothes_id[0]+ ')=', all_probs[0][0])
    print ('Pr(' +clothes_id[1]+ ')=', all_probs[0][1])
    
# Hyper Parameter tuning    
def calculateHyperParametersGS(trainFeatures, trainLabels):
    print ("----- CRL Hyper Parameters -----")
    clf = MLPClassifier(#solver='adam',
                        activation='relu',
                        #batch_size=22,
                        max_iter = 200,
                        alpha=1e-6,
                        #warm_start=True,
                        #hidden_layer_sizes=(100, 60),
                        #random_state=1,
                        verbose=True,
                        #learning_rate_init = 0.0005
                        )

    param_grid = [{'batch_size': [32, 31, 30, 29, 28, 27, 26],
                   'warm_start' : [False,True],
                   'random_state':[0,1],
                   'hidden_layer_sizes' : [(100, 60) ,(100, 60, 50)],
                   'solver' : ['lbfgs', 'sgd', 'adam'],
                   'learning_rate_init' : [0.0005, 0.0004, 0.0003, 0.00005]
                 }]

    # search
    rs = GridSearchCV(clf, param_grid, n_jobs=-1, scoring='f1_micro')
    rs.fit(trainFeatures, trainLabels)
    print('best params:', rs.best_params_)
    print('best CV score:', rs.best_score_)

# Train and test set only for T-shirt and trousers 
def twoclasses(path, kind='train'):
    """Load MNIST data from `path`"""
    labels_path = os.path.join(path,
                               '%s-labels-idx1-ubyte.gz'
                               % kind)
    images_path = os.path.join(path,
                               '%s-images-idx3-ubyte.gz'
                               % kind)

    with gzip.open(labels_path, 'rb') as lbpath:
        labels = np.frombuffer(lbpath.read(), dtype=np.uint8,
                               offset=8)

    with gzip.open(images_path, 'rb') as imgpath:
        images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                               offset=16).reshape(len(labels), 784)
    
    x = []
    y = []
    
    for i,image in enumerate(images):
        if labels[i] == 0 or labels[i] == 1:
            x.append(images[i])
            y.append(labels[i])
    x = np.array(x)
    y = np.array(y)
     
    return x, y

if __name__ == '__main__':
    print("For MLP training procedure press 1")
    print("For MLP load params        press 2")
    ch = int(input("1/2: "))
    if ch == 1:
        X_train, Y_train = twoclasses('samples', kind='train')
        X_test, Y_test = twoclasses('samples', kind='t10k')
        x = MLP(X_train, Y_train, X_test, Y_test)
    else:
        #clf = pickle.load(open("MLP_10classes.p", "rb"))
        clf = pickle.load(open("MLP_2classes.p", "rb"))
        x_test = load_photo()
        predict(clf, x_test)

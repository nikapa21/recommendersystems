# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework import status
from PIL import Image
import numpy as np
import cPickle as pickle
from datetime import datetime
import requests
from django.core.files.storage import default_storage


class Classification(APIView):
    #permission_classes = [IsAuthenticated, ]
    def post(self, request, format=None):
        
        # Receive and Store image to server
        file = request.data.get('picture')
        filename = str(file)

        with default_storage.open('image/' + filename, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
        
        # Load image for classification
        img = Image.open("image/tshirt.png")
        
        img = img.convert('L')     # Reshape photo to 28*28 pixel and greyscale
        img = img.resize((28, 28))
        data = list(img.getdata()) # convert image data to a list of integers
        for i,pix in enumerate(data):
            if pix > 255:
                data[i] = 0
        x = np.array(data)
        x = x.reshape(1, -1)
        
        # Predict Class
        mlp = pickle.load(open("../MLP_2classes.p", "rb"))
        clothes_id = {0: 'T-shirt/top',
                      1: 'Trouser'}
        y_pred = mlp.predict(x)
        all_probs = mlp.predict_proba(x)
        return JsonResponse({'class': clothes_id[y_pred[0]]})

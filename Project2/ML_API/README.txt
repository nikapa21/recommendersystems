This folder contains the server files and the restfull api implementation using Django and rest_framework.

--- How does it work? ---

1) Android ---- send image (request to 127.0.0.1:8000/classify/) ----> REST API
2) REST API receives the request and classify the image (T-shirt or trouser) based on the pre-trained MLP
3) Then, sends back a response to the source of the request (Android) with the following json message:
{class:T-shirt} or {class:Trouser}.

To start the server server run the command: python manage.py runserver

Important API File's (structure):

ML_API
    |
     -- manage.py
    |
     -- image (folder where the incoming image is stored) 
    |
     --ML_API
            |
            --settings.py
            --urls.py (endpoints name)
    | 
     --photo_classification
            |
            --views.py (endpoint code for receiving and classifing the image)
            
